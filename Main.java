/* Desenvolvimento de Quiz para a disciplina Programação III,
 * ministrada pelo professor Samuel Oliveira, no semestre 2018.1, do curso
 * Ciência da Computação, UNIFAP.
 *
 * Lucas Matheus Araújo Trajano de Souza
 * Josemar Ferreira Gama
 * Edivaldo da Silva Almeira Júnior
 *
 * Junho de 2018
 */

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.util.Scanner;
import java.awt.BorderLayout;

public class Main{
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                JFrame frame = new MainFrame("Quiz de Ciência da Computação");
                frame.setSize(500, 400);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }
}

class MainFrame extends JFrame{
    private static final long serialVersionUID = 1L;
    public MainFrame(String title){
        super(title);
        setLayout(new BorderLayout());
    }
}
        //Scanner scanner = new Scanner(System.in);
        //int count = 0;
        //System.out.println("Bem-vindo ao Quiz de Ciência da Computação!\n\n");
        //System.out.println("Em Java, que é herança?\n" +
                //"1. Teste\n" + 
                //"2. Teste\n" +
                //"3. Teste\n" +
                //"4. Teste\n\n");
        //String opcao1 = scanner.nextLine();
        //switch(Integer.valueOf(opcao1)){
            //case 1: 
                //count += 1; 
                //break;
        //}
    //}
//}

//class Perguntas{
//}


